﻿using UnityEngine;
using System.Collections;

public class bounceEffect : MonoBehaviour {

	void OnEnable()
    {
        StartCoroutine(DeactivateSelf());
    }

    private IEnumerator DeactivateSelf()
    {
        yield return new WaitForSeconds(1);

        gameObject.SetActive(false);
    }
}
