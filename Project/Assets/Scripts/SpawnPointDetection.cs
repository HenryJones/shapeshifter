﻿using UnityEngine;
using System.Collections;

public class SpawnPointDetection : MonoBehaviour {

    public bool avaliable = true;

    public void SetUnavaliable()
    {
        avaliable = false;
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            avaliable = true;
        }
    }
}
