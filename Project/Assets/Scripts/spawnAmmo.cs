﻿using UnityEngine;
using System.Collections;

public class spawnAmmo : MonoBehaviour {

    private GameObject[] spawnPoints;
    private GameObject[] spawnAreas;

    public float spawnTimer = 5.0f;

    public GameObject[] ammoShpaes;
	
    private string ammoType;

    public int count = 0;
    private bool canSpawn = true;

    void Start ()
    {
        spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        spawnAreas = GameObject.FindGameObjectsWithTag("SpawnArea"); 

        StartCoroutine(SpawnShapes());
	}
	

    IEnumerator SpawnShapes()
    {
        yield return new WaitForSeconds(spawnTimer);

        if (canSpawn == true)
        {
            Vector3 randomPoint = GetSpawnPoint();

            int randomShape = Random.Range(1, 4);


            if (randomShape == 1)
            {
                ammoType = "CircleAmmo";
            }
            else if (randomShape == 2)
            {
                ammoType = "SquareAmmo";
            }
            else if (randomShape == 3)
            {
                ammoType = "TriangleAmmo";
            }

            GameObject Ammo = PoolManager.current.GetPooledObject(ammoType);

            if(Ammo != null)
            {
                Ammo.transform.position = randomPoint;
                Ammo.SetActive(true);
            }
      

        }
        StartCoroutine(SpawnShapes());

    }

    private Vector3 GetSpawnPoint()
    {

        int spawner = Random.Range(0, spawnAreas.Length);

        float minwidth = spawnAreas[spawner].gameObject.GetComponent<Renderer>().bounds.min.x + 2;
        float maxWidth = spawnAreas[spawner].gameObject.GetComponent<Renderer>().bounds.max.x - 2;
        float minHeight = spawnAreas[spawner].gameObject.GetComponent<Renderer>().bounds.min.z + 2;
        float maxHeight = spawnAreas[spawner].gameObject.GetComponent<Renderer>().bounds.max.z - 2;
        

        float spawnPosx = Random.Range(minwidth, maxWidth);
        float spawnPosz = Random.Range(minHeight, maxHeight);

        Vector3 spawnPos = new Vector3(spawnPosx, 0, spawnPosz);

        return spawnPos; 
    }
}
