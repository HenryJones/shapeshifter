﻿using UnityEngine;
using System.Collections;

public class StopRotating : MonoBehaviour
{

    // Use this for initialization
    private GameObject Player1;
    private GameObject Player2;
    private Vector3 offset;
    Quaternion newOffset;

    void Start()
    {
        Player1 = GameObject.FindGameObjectWithTag("Player1");
        Player2 = GameObject.FindGameObjectWithTag("Player2");
        offset = transform.rotation.eulerAngles - Player1.transform.rotation.eulerAngles;
        newOffset = transform.rotation;
        newOffset.eulerAngles = offset + Player1.transform.position;
    }
    void Update()
    {
        if (gameObject.name == "Player1")
        {
            transform.position = Player1.transform.position + new Vector3(1.5f, 5, 1.25f);

        }
        else if (gameObject.name == "Player2")
        {
            transform.position = Player2.transform.position + new Vector3(1.5f, 5, 1.25f);

        }
    }
}