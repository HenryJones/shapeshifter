﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ControllerEventManager : MonoBehaviour {

    private bool m_ControllerInput = false; // a check to see if both controllers are connected and setup
    private string m_XboxOneName = "Controller (Xbox One For Windows)";
    private string m_Xbox360Name = " Xbox 360 For Windows (Controller)";
    private bool m_Controller1 = false; //check to see if the first controller is plugged in
    private bool m_Controller2 = false; // check to see if the second controller is plugged in
    private string m_Controller1Name = ""; // stores the first controller's name
    private string m_Controller2Name = ""; // stores the second controller's name

    private GameObject m_eventManager;
    public GameObject m_firstButton;

    public Button m_dummy = null;

    public Button m_resume;
    public Button m_backButton;

    private bool m_showingControls = false; // keeps track if the controls are being displayed or not;
    private bool m_usingMouse = true; // a bool to detect if the mouse is being used;


    void Start ()
    {
        m_eventManager = GameObject.FindGameObjectWithTag("EventSystem");

        m_Controller1 = false;
        m_Controller2 = false;

        string[] names = Input.GetJoystickNames(); // stores the names of all the controllers connected

        if (names.Length > 1) // checks to see if there are atleast 2 controllers
        {
            if (names[0].Length == 33)
            {
                m_Controller1 = true;
                m_Controller1Name = names[0];
            }
            if (names[1].Length == 33)
            {
                m_Controller2 = true;
                m_Controller2Name = names[1];

            }
            if (m_Controller1 == true || m_Controller2 == true) // if both controllers are connected set controller input to true
            {
                m_ControllerInput = true;
                m_usingMouse = false;
            }
        }

        if(m_ControllerInput == true)
        {
            if(m_showingControls == false)
            {
                m_eventManager.GetComponent<EventSystem>().firstSelectedGameObject = m_firstButton;
            }
           
        }
    }

    void Update()
    {
        if (Input.GetButton("ControllerStartButton")) // detects start button
        {
            PauseMenuOpen();
        }

        if (Input.GetAxis("Controller1Vertical") != 0 || Input.GetAxis("Controller2Vertical") != 0 || Input.GetAxis("Controller1Horizontal") != 0 || Input.GetAxis("Controller2Horizontal") != 0)// detects input from controller
        {
            if(m_usingMouse == true)
            {
                if(m_showingControls == false)
                {
                    m_eventManager.GetComponent<EventSystem>().SetSelectedGameObject(m_firstButton);
                }
                else
                {
                    m_backButton.Select();
                }

                Cursor.visible = false;
                m_usingMouse = false;
            }
        }

        if(Input.GetAxis("Mouse X") < 0 || Input.GetAxis("Mouse X") > 0 || Input.GetAxis("Mouse Y") < 0 || Input.GetAxis("Mouse Y") > 0) // detect mouse movement
        {  
            m_usingMouse = true;
            Cursor.visible = true;
            m_eventManager.GetComponent<EventSystem>().SetSelectedGameObject(null); // if the mouse is being used stop selecting the buttons for controllers
        }
    }

    public void PauseMenuOpen()
    {
        if (m_ControllerInput == true && m_usingMouse == false)
        {
            m_dummy.Select(); // selects a different button first to make sure the resume button is activated when selected (Unity bug)
            m_resume.Select();
        }
    }

    public void ShowControls()
    {
        m_showingControls = true;
        if (m_backButton != null)
        {
            m_backButton.Select();
        }
    }

    public void HideControls()
    {
        m_showingControls = false;
        m_dummy.Select(); // selects a different button first to make sure the resume button is activated when selected (Unity bug)
        m_resume.Select();
        // m_usingMouse = true; // this is here so that when the controls are hidden the check for controller input will run and it will select resume.
    }
}
