﻿using UnityEngine;
using System.Collections;
using HJ;

public class WallDectector : MonoBehaviour {

    private GameObject m_Player;

	void Start ()
    {
        if (name == "Wall Detector 1")
        {
            m_Player = GameObject.FindGameObjectWithTag("Player1");
        }
        else if(name == "Wall Detector 2")
        {
            m_Player = GameObject.FindGameObjectWithTag("Player2");
        }
	}
	

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Wall")
        {
            if(m_Player != null)
            {
                m_Player.GetComponent<PlayerScript>().SetCanShoot(false);
            }
            else
            {
                print("cannot find player");
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Wall")
        {
            if (m_Player != null)
            {
                m_Player.GetComponent<PlayerScript>().SetCanShoot(true);
            }
            else
            {
                print("cannot find player");
            }
        }
    }
}
