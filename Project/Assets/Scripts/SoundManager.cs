﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour {

    private static SoundManager instance = null;
    public static SoundManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (SoundManager)FindObjectOfType(typeof(SoundManager));
            }
            return instance;
        }
    }

    private AudioSource m_AudioSource;

    public AudioClip m_menuMusic;
    public AudioClip m_gameMusic;

    private string currentScene = "";

    void Awake()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        currentScene = SceneManager.GetActiveScene().name;

        m_AudioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().name != currentScene)
        {
            currentScene = SceneManager.GetActiveScene().name;
            CheckLevel(); 
        }
    }


    private void CheckLevel()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            PlayMenuMusic();
        }
        else if(SceneManager.GetActiveScene().buildIndex == 2)
        {
            PlayMenuMusic();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 4)
        {
            PlayMenuMusic();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 9)
        {
            PlayMenuMusic();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 10)
        {
            PlayMenuMusic();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 11)
        {
            PlayMenuMusic();
        }
        else if (SceneManager.GetActiveScene().buildIndex == 12)
        {
            PlayMenuMusic();
        }
        else
        {
            PlayGameMusic();
        }

    }

    private void PlayMenuMusic()
    {
        if(m_AudioSource.clip != m_menuMusic)
        {
            m_AudioSource.clip = m_menuMusic;
            m_AudioSource.Play();
        }
       
    }

    private void PlayGameMusic()
    {
        if (m_AudioSource.clip != m_gameMusic)
        {
            m_AudioSource.clip = m_gameMusic;
            m_AudioSource.Play();
        }
    }
}

