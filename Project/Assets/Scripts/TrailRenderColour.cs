﻿using UnityEngine;
using System.Collections;

public class TrailRenderColour : MonoBehaviour {

    public Material redTrail;
    public Material blackTrail;


    void OnEnable()
    {
        GetComponentInChildren<TrailRenderer>().Clear();
    }

    public void GetPlayer(string player)
    {
        
        if (player == "Player1")
        {
            GetComponentInChildren<TrailRenderer>().material = redTrail;
        }
        else
        {
            GetComponentInChildren<TrailRenderer>().material = blackTrail;
        }

        GetComponentInChildren<TrailRenderer>().Clear();
    }
}
