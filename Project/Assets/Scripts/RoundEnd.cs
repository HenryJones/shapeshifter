﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class RoundEnd : MonoBehaviour
{
    public AudioClip m_dieSound;
    private AudioSource m_AudioSource;

    public Text Player1ScoreText;
    public Text Player2ScoreText;

    private static int Player1Score = 0;
    private static int Player2Score = 0;

    void OnEnable()
    {
        m_AudioSource = GetComponent<AudioSource>();

        if(Player1ScoreText != null)
        {
            Player2ScoreText.text = Player2Score.ToString();
            Player1ScoreText.text = Player1Score.ToString();
        }
      
    }

    private void Update()
    {
        if(SceneManager.GetActiveScene().name == "LevelSelect")
        {
            Player1Score = 0;
            Player2Score = 0;
        }
    }

    public void DisplayWinner(string Player)
    {
        if(Player == "Player1")
        {
            Player2Score++;
            Player2ScoreText.text = Player2Score.ToString();
            StartCoroutine(StartNewRound());
        }
        else if(Player == "Player2")
        {
            Player1Score++;
            Player1ScoreText.text = Player1Score.ToString();
            StartCoroutine(StartNewRound());
        }

        m_AudioSource.clip = m_dieSound;
        m_AudioSource.Play();
    }

    private IEnumerator StartNewRound()
    {
        yield return new WaitForSeconds(2);

        int sceneID = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(sceneID); // change to current level
    }
}
