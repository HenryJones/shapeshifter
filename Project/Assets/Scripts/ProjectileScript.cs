﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour
{

    [SerializeField]
    private float m_speed = 5;
    [SerializeField]
    private float m_destroyTime = 30;

    [SerializeField]
    private float m_height;

    private Rigidbody m_rBody;

    private Vector3 m_velocity;
    private float m_velMag;

    public Material m_player1Mat;
    public Material m_player2Mat;

    private Material[] m_player1Colours = new Material[2];
    private Material[] m_player2Colours = new Material[2];

    private GameObject m_GameManager;

    private int m_bounceLimit = 6;

    private float m_CollisionResetTime = 1.0f;
    private bool m_CanCollide = true;

    //sounds
    public AudioClip BounceSound;
    private AudioSource m_AudioSource;

    void OnEnable()
    {
        m_GameManager = GameObject.FindGameObjectWithTag("GameManager");

        m_AudioSource = GetComponent<AudioSource>();

        if (m_rBody == null)
            m_rBody = GetComponent<Rigidbody>();

        m_rBody.velocity = Vector3.zero;
        m_rBody.angularVelocity = Vector3.zero;
        m_velocity = transform.forward * m_speed;
        m_rBody.velocity = m_velocity;
        m_destroyTime = 1000;

        m_velMag = 87f;

         StartCoroutine(DeActivate());

        m_player1Colours[0] = m_player1Mat;
        m_player1Colours[1] = m_player1Mat;
        m_player2Colours[0] = m_player2Mat;
        m_player2Colours[1] = m_player2Mat;

        m_CanCollide = true;
    }

    void OnCollisionEnter(Collision other)
    {
        if(m_CanCollide == true)
        {
            if(gameObject == isActiveAndEnabled)
            {
                StartCoroutine(ResetCollideBool());
            }
          //  m_CanCollide = false; // stops the projectle from hitting two colliders at once (if on a corner)

            Vector3 sumVector = new Vector3();

           // Debug.Log(sumVector);

          for(int i = 0; i < other.contacts.Length; i++)
          {
                sumVector += other.contacts[i].normal;
                Debug.Log("points = " + other.contacts.Length + "        sum" + sumVector);
            }

            Vector3 newAngle = Vector3.Reflect(m_velocity.normalized, other.contacts[0].normal); // gets the new angle the projectile will be facing 
            m_velocity = newAngle * m_velMag; // change the velocity 
            m_rBody.velocity = m_velocity; // apply that to the rbody

            PlayBounceSound(); 

            GameObject bounceEffect = PoolManager.current.GetPooledObject("BounceEffect"); 
            bounceEffect.transform.position = transform.position;
            bounceEffect.SetActive(true);
        }
    }

    private IEnumerator ResetCollideBool()
    {
         yield return new WaitForSeconds(m_CollisionResetTime);
         m_CanCollide = true;
    }

    private void SetHeight()
    {
        transform.position = new Vector3(transform.position.x, m_height, transform.position.z);
    }

    private IEnumerator DeActivate()
    {
        yield return new WaitForSeconds(m_destroyTime); // MIGHT HAVE PROBLEMS USING A VARIABLE 
        if(gameObject != null && gameObject.activeInHierarchy)
        {
            gameObject.SetActive(false);
        }
        m_rBody.velocity = Vector3.zero;
        m_rBody.angularVelocity = Vector3.zero;
    }

    public void DeactivateOnHit()
    { 
        gameObject.SetActive(false);
        m_rBody.velocity = Vector3.zero;
        m_rBody.angularVelocity = Vector3.zero;
    }

    public void GetPlayer(string player)
    {
        if (player == "Player1")
        {
            GetComponentInChildren<MeshRenderer>().materials = m_player1Colours;
        }
        else
        {
            GetComponentInChildren<MeshRenderer>().materials = m_player2Colours;
        }
    }
    private void PlayBounceSound()
    {
        if(m_GameManager.GetComponent<BounceSoundLimit>().GetBounceAmount() < m_bounceLimit)
        {
            m_AudioSource.clip = BounceSound;
            if(m_AudioSource == isActiveAndEnabled)
            {
                m_AudioSource.Play();
                m_GameManager.GetComponent<BounceSoundLimit>().AddSoundAmount();
            }
           
           
            if (gameObject == isActiveAndEnabled)
            {
                StartCoroutine(BounceSoundEnded());
            }
            else
            {
                m_GameManager.GetComponent<BounceSoundLimit>().MinusSoundAmount();
            }
               
        }
    }

    private IEnumerator BounceSoundEnded()
    {
        yield return new WaitForSeconds(m_AudioSource.clip.length);

        if(gameObject == isActiveAndEnabled)
        m_GameManager.GetComponent<BounceSoundLimit>().MinusSoundAmount();
    }
}

