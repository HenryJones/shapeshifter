﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace HJ
{
    public class PauseManager : MonoBehaviour
    {

        private bool paused = false;
        public GameObject pauseMenu;

        private GameObject player1;
        private GameObject player2;

        public GameObject controlItems;
        public GameObject m_menuItems;
        public GameObject m_controllerImage;

        private bool showingControls = false;

        private GameObject m_EventManager;
        private float m_menuTimer = .25f;
        private bool m_menuReset = true;

        void Start()
        {
            m_EventManager = GameObject.FindGameObjectWithTag("EventSystem");
            paused = false;
            player1 = GameObject.FindGameObjectWithTag("Player1");
            player2 = GameObject.FindGameObjectWithTag("Player2");
            Time.timeScale = 1;
        }

        void OnEnable()
        {
            paused = false;
            Time.timeScale = 1;
        }

        void Update()
        {
            if (m_menuReset == true)
            {
                if (Input.GetKeyDown(KeyCode.Escape) || Input.GetButton("ControllerStartButton"))
                {
                    m_menuReset = false;
                    
                    paused = !paused;
                   
                    if (paused == true)
                    {
                        Pause();
                    }
                    else
                    {
                        if (showingControls == false) // if controls image isnt showing
                        {
                            UnPause();
                        }
                    }
                    StartCoroutine(ResetMenuTimer());
                }   
            }
        }

        private void setPlayerPause()
        {
            if (player1 != null && isActiveAndEnabled)
            {
                player1.GetComponent<PlayerScript>().SetPaused(paused);
            }

            if (player2 != null && isActiveAndEnabled)
            {
                player2.GetComponent<PlayerScript>().SetPaused(paused);
            }
        }

        private void Pause()
        {
            paused = true;
            setPlayerPause();
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
            m_menuItems.SetActive(true);
            if (m_EventManager)
            { 
              m_EventManager.GetComponent<ControllerEventManager>().PauseMenuOpen();
            }
        }

        public void UnPause()
        {
            paused = false;
            setPlayerPause();
            Time.timeScale = 1;
            pauseMenu.SetActive(false);
        }

        public void ShowControls()
        {
            controlItems.SetActive(true);
            m_menuItems.SetActive(false);
            showingControls = true;
            m_EventManager.GetComponent<ControllerEventManager>().ShowControls();
        }

        public void HideControls()
        {
            controlItems.SetActive(false);
            showingControls = false;
            m_controllerImage.SetActive(false);
            m_menuItems.SetActive(true);
            m_EventManager.GetComponent<ControllerEventManager>().HideControls();
            
        }

        private IEnumerator ResetMenuTimer()
        {
           yield return new  WaitForSecondsRealtime(m_menuTimer);
            m_menuReset = true;
        }

        public void ShowControllerControls()
        {
            m_controllerImage.SetActive(true);
        }

        public void HideControllerControls()
        {
            m_controllerImage.SetActive(false);
        }
    }
}
