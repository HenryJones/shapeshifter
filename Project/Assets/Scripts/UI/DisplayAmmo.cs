﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DisplayAmmo : MonoBehaviour {

    public Text Ammo1;
    public Text Ammo2;

    private int[] ammo1;
    private int[] ammo2;

    private int cycle1 = 1;
    private int cycle2 = 1;

    void Start()
    {
        ammo1 = new int[3] { 1, 1, 1 };
        ammo2 = new int[3] { 1, 1, 1 };
    }

    void OnEnbale()
    {
        ammo1 = new int[3] { 1, 1, 1 };
        ammo2 = new int[3] { 1, 1, 1 };
    }
    public void UpdateAmmo(int[] ammo, string name, int cycle) // comes from player after gain anad loss of ammo
    {
        if(name == "Player1")
        {
            ammo1 = ammo;
            cycle1 = cycle;
        }
        else if(name == "Player2")
        {
            ammo2 = ammo;
            cycle2 = cycle;
        }
        else
        {
            print("name not found");
        }

        ShowText();

    }

    private void ShowText()
    {
        if (ammo1 != null)
        {
            Ammo1.text = ammo1[cycle1 - 1].ToString();
        }
        if (ammo2 != null)
        {
            Ammo2.text = ammo2[cycle2 - 1].ToString();
        }

    }
}
