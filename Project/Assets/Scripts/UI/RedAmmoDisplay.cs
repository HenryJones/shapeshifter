﻿using UnityEngine;
using System.Collections;

public class RedAmmoDisplay : MonoBehaviour {

    private GameObject Player1;

	void OnEnable()
    {
        Player1 = GameObject.FindGameObjectWithTag("Player1");
    }

    void Start()
    {
        Player1 = GameObject.FindGameObjectWithTag("Player1");
    }

	void Update ()
    {
        transform.position = Player1.transform.position + new Vector3(1.2f, 2, 1.2f);
	}
}
