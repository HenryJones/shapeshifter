﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

	public void PlayButton()
    {
        //Application.LoadLevel("LevelSelect");
        SceneManager.LoadScene("LevelSelect");
    }
    public void ControlButton()
    {
        SceneManager.LoadScene("ControlScreen");
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
