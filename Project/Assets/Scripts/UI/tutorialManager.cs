﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class tutorialManager : MonoBehaviour {

    private GameObject m_player1;
    private GameObject m_player2;
    public GameObject m_ammoPickup;

    void Start()
    {
        m_player1 = GameObject.FindGameObjectWithTag("Player1");
        m_player2 = GameObject.FindGameObjectWithTag("Player2");

        if (SceneManager.GetActiveScene().name == "TutorialShooting")
        {
            StartCoroutine(ResetPlayers1());
        }
        else if (SceneManager.GetActiveScene().name == "TutorialSwitching")
        {
            StartCoroutine(ResetPlayers2());
        }
        else if (SceneManager.GetActiveScene().name == "TutorialResupplying")
        {
            StartCoroutine(ResetPlayers3());
        }

    }


    private IEnumerator ResetPlayers1()
    {
        m_player1.SetActive(true);
        m_player2.SetActive(true);

        yield return new WaitForSeconds(4);

       // m_player1.SetActive(false);
        m_player2.SetActive(false);

        yield return new WaitForSeconds(1.5f);

        m_player1.SetActive(false);
        yield return new WaitForSeconds(0.2f);

       
        StartCoroutine(ResetPlayers1());
    }

    private IEnumerator ResetPlayers2()
    {
        m_player1.SetActive(true);
        m_player2.SetActive(true);

        yield return new WaitForSeconds(3);

        m_player1.SetActive(false);
        m_player2.SetActive(false);

        StartCoroutine(ResetPlayers2());
    }

    private IEnumerator ResetPlayers3()
    {
        m_player1.SetActive(true);
        m_ammoPickup.SetActive(true);

        yield return new WaitForSeconds(4);

        m_player1.SetActive(false);

        StartCoroutine(ResetPlayers3());
    }
}
