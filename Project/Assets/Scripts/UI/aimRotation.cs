﻿using UnityEngine;
using System.Collections;

public class aimRotation : MonoBehaviour {

    private float m_aimX;
    private float m_aimY;
    private float m_angle;

    private Rigidbody m_rigidbody;

    // Use this for initialization
    void Start ()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (tag == "Player1")
        //{
        //    m_aimX = Input.GetAxis("Controller1HorizontalAim");
        //    m_aimY = Input.GetAxis("Controller1VerticalAim");
        //}
        //else
        //{
        //    m_aimX = Input.GetAxis("Controller2HorizontalAim");
        //    m_aimY = Input.GetAxis("Controller2VerticalAim");
        //}

        //float m_aimAngle = Mathf.Atan2(m_aimX, m_aimY);

        //if (m_aimX != 0 || m_aimY != 0) // if theres a rotation
        //{
        //    float angleD = m_aimAngle * Mathf.Rad2Deg;

        //    //Vector3 currentRot = m_Transform.rotation.eulerAngles;
        //    Vector3 targetRot = new Vector3(0, angleD, 0);

        //    m_rigidbody.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(targetRot), 10f);

        //}
    }

    public void GetRotation(Vector3 _newTarget, float _rotationSpeed)
    {
       m_rigidbody.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(_newTarget), _rotationSpeed);
    }
}
