﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelSelectManager : MonoBehaviour {

    public void LoadLevel1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void LoadLevel2()
    {
        SceneManager.LoadScene("Level2");
    }

    public void LoadLevel3()
    {
        SceneManager.LoadScene("Level3");
    }

    public void LoadLevel4()
    {
        SceneManager.LoadScene("Level4");
    }

    public void LoadLevel5()
    {
        SceneManager.LoadScene("Level5");
    }

    public void LoadLevel6()
    {
        SceneManager.LoadScene("Level6");
    }

    public void LoadMainMenu()
    {
        if(Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene("Menu");
    }

    public void LevelSelect()
    {
        if (Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene("LevelSelect");
    }

    public void LoadControlsPC()
    {
        if (Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene("ControlScreen");
    }

    public void LoadControlsController()
    {
        if (Time.timeScale != 1)
        {
            Time.timeScale = 1;
        }
        SceneManager.LoadScene("ControlScreenController");
    }

    public void Quit()
    {
        Application.Quit();
    }

    //Tutorial Levels

    public void LoadShootingTutorial()
    {
        if (Time.timeScale != 1)
            Time.timeScale = 1;

        SceneManager.LoadScene("TutorialShooting");
    }

    public void LoadSwitchingTutorial()
    {
        if (Time.timeScale != 1)
            Time.timeScale = 1;

        SceneManager.LoadScene("TutorialSwitching");
    }

    public void LoadResupplyingTutorial()
    {
        if (Time.timeScale != 1)
            Time.timeScale = 1;

        SceneManager.LoadScene("TutorialResupplying");
    }
    

}
