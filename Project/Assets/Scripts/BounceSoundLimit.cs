﻿using UnityEngine;
using System.Collections;

public class BounceSoundLimit : MonoBehaviour {

    private int m_bounceCount = 0;

	public void AddSoundAmount()
    {
        m_bounceCount++;
    }

    public void MinusSoundAmount()
    {
        m_bounceCount--;
    }

    public int GetBounceAmount()
    {
        return m_bounceCount;
    }
}
