﻿using UnityEngine;
using System.Collections;

namespace HJ
{
    public class PlayerScript : MonoBehaviour
    {

        [SerializeField]
        private int m_health = 100;

        private int m_cycle = 1; // what shape the player is set to
        [SerializeField]
        private float m_Speed = 0.2f;

        [SerializeField]
        private float m_rotationSpeed = 50.0f;

        private Transform m_Transform;
        private Rigidbody m_rigidbody;

        private float m_rotationY;
        private float m_rotation;

        //shapes
        public GameObject circle;
        public GameObject square;
        public GameObject triangle;

        public GameObject circleCollecter;
        public GameObject triangleCollecter;
        public GameObject squareCollecter;

        //spawn
        public GameObject spawnPoint; // where the projectiles fire from.

        public GameObject aimer;

        //GameManager
        private GameObject gameManger; //controls the game

        private string m_inputH; // stores the name of the input used for each player
        private string m_inputV; // sotres the name of the input used for each player
    //    private string m_inputHAim;
     //   private string m_inputVAim;

        private Vector3 m_moveInput;
        private Vector3 m_direction;
        private float m_dirX; // which way to face
        private float m_dirY;
        private float m_dirAngle;

        private float m_aimX; // which way to aim
        private float m_aimY;
        private float m_aimAngle;

        private bool m_paused = false; // bool to store if game is pasued

        private bool m_kickBack = false; // bool to check if kickback on the player is active

        //Ammo
        [SerializeField]
        private int[] m_ammo = new int[3]; // responsible for holding the ammo amount of each of the 3 projectiles

        //sounds
        public AudioClip shootSound; // played when firing
        public AudioClip AmmoPickup; // played when picking up ammo

        private AudioSource m_AudioSource;

        [SerializeField]
        private bool m_canShoot = true; // determines if the player can shoot or not.

        //controller input
        private bool m_ControllerInput = false; // a check to see if both controllers are connected and setup
        private string m_XboxOneName = "Controller (Xbox One For Windows)";
        private string m_Xbox360Name = " Xbox 360 For Windows (Controller)";
        private bool m_Controller1 = false; //check to see if the first controller is plugged in
        private bool m_Controller2 = false; // check to see if the second controller is plugged in
        private string m_Controller1Name = ""; // stores the first controller's name
        private string m_Controller2Name = ""; // stores the second controller's name
        private int m_xboxNameLength = 33; //length of the name of either of xbox joysticks

        private bool m_canTriggerShoot = true; // a bool to see if the code will accept the input from the controller triggers
        private bool m_canTriggerSwitch = true; // a bool to see if the code will accept the input from the controller triggers
        private float m_TriggerInputResetTime = 0.3f; // how long until the trigger input can be registed again

        void Start()
        {
            m_Controller1 = false;
            m_Controller2 = false;

            //Controller input
            string[] names = Input.GetJoystickNames(); // stores the names of all the controllers connected

            if(names.Length > 1) // checks to see if there are atleast 2 controllers
            {
                if (names[0].Length == 33)
                {
                    m_Controller1 = true;
                    m_Controller1Name = names[0];    
                }
                if (names[1].Length == 33)
                {
                    m_Controller2 = true;
                    m_Controller2Name = names[1];

                }

                if (m_Controller1 == true && m_Controller2 == true) // if both controllers are connected set controller input to true
                {
                    m_ControllerInput = true;
                }
            }

            DetermineInput(); // detrmines what input to use (keyboard and mouse or controllers)

            m_cycle = 1;

            m_Transform = GetComponent<Transform>();
            m_rigidbody = GetComponent<Rigidbody>();

            gameManger = GameObject.FindGameObjectWithTag("GameManager");

            m_AudioSource = GetComponent<AudioSource>();
        }

        void OnEnable()
        {
            gameManger = GameObject.FindGameObjectWithTag("GameManager");

            m_cycle = 1;

            //reset ammo
            m_ammo[0] = 1;
            m_ammo[1] = 1;
            m_ammo[2] = 1;

            gameManger.GetComponent<DisplayAmmo>().UpdateAmmo(m_ammo, name, m_cycle);
            m_canShoot = true;
        }

        void FixedUpdate()
        {
            Rotate();
            Move();
        }

        void Update()
        {
            if (m_paused == false) 
            {
                CheckCycle(); // checks to see what shape the player is set to

                float shootInput = 0;

                if(m_ControllerInput == true) // checks which player input is needed
                {
                    if (tag == "Player1")
                    {
                        shootInput = Input.GetAxis("Controller1Triggers");
                    }
                    else if (tag == "Player2")
                    {
                        shootInput = Input.GetAxis("Controller2Triggers");
                    }
                }  

                if (tag == "Player1")
                {   
                    if(m_canTriggerShoot == true)
                    {
                        if (Input.GetKeyDown(KeyCode.Space) || (shootInput == -1))
                        {
                            Fire();
                            m_canTriggerShoot = false; // stops the player from being able to shoot continuously.
                            StartCoroutine(ResetShootTriggerInput()); // starts a timer to allow the player to shoot again.
                        }
                    }
                }
                else if (tag == "Player2")
                {
                    if (m_canTriggerShoot == true) // add player 2 firing here
                    {
                        if (Input.GetMouseButtonDown(0) || (shootInput == -1))
                        {
                            Fire();
                            m_canTriggerShoot = false; // stops the player from being able to shoot continuously.
                            StartCoroutine(ResetShootTriggerInput()); // starts a timer to allow the player to shoot again.
                        }
                    }
                }
                else
                {
                    print("Error cant find tag");
                }
            }
        }

        

        private void CheckCycle()
        {
            float switchInput = 0;

            if(m_ControllerInput == true) // if controllers are being used
            {
                if (tag == "Player1")
                {
                    switchInput = Input.GetAxis("Controller1Triggers");
                }
                else
                {
                    switchInput = Input.GetAxis("Controller2Triggers");
                }
            }
            
            if (m_canTriggerSwitch == true) // checks if the trigger input is accepted
            {
                if (tag == "Player1")
                {
                    if (Input.GetKeyDown(KeyCode.M) || switchInput == 1) // input to switch shapes
                    {
                        if (m_cycle == 3) // if the array is at the end the index is wrapped back to the start
                        {
                            m_cycle = 1;
                        }
                        else
                        {
                            m_cycle++; // increment the index for the cycle array by 1
                        }
                        DeactivateShapes(); // deactivates all shapes
                        SetShape(); // sets the shape to be the shape that has just been set
                        gameManger.GetComponent<DisplayAmmo>().UpdateAmmo(m_ammo, name, m_cycle); // updates what ammo is being displayed

                        m_canTriggerSwitch = false;
                        StartCoroutine(ResetSwitchTriggerInput());
                    }
                }
                else if (name == "Player2")
                {
                    if (Input.GetMouseButtonDown(1) || switchInput == 1) // input to switch shapes
                    {
                        if (m_cycle == 3)
                        {
                            m_cycle = 1;
                        }
                        else
                        {
                            m_cycle++;
                        }
                        DeactivateShapes();
                        SetShape();
                        gameManger.GetComponent<DisplayAmmo>().UpdateAmmo(m_ammo, name, m_cycle);

                        m_canTriggerSwitch = false; // stops the player from being able to switch continuously.
                        StartCoroutine(ResetSwitchTriggerInput()); //starts a timer so that the player will be able to switch again
                    }
                }
                else
                {
                    print("Error cant find name");
                }
               
            }

        }

        private void SetShape()     // set the current shape to active 
        {
            if (m_cycle == 1) // 1 = circle 
            {
                circle.SetActive(true);
                circleCollecter.SetActive(true);
            }
            else if (m_cycle == 2) //  2 = square 
            {
                square.SetActive(true);
                squareCollecter.SetActive(true);
            }
            else if (m_cycle == 3) //  3 = triangle
            {
                triangle.SetActive(true);
                triangleCollecter.SetActive(true);
            }
        }

        private void DeactivateShapes() // deactivates all shape ready for the new shape to be set
        {
            circle.SetActive(false);
            square.SetActive(false);
            triangle.SetActive(false);

            circleCollecter.SetActive(false);
            squareCollecter.SetActive(false);
            triangleCollecter.SetActive(false);
        }

        private void Move()
        {

            //float z = Input.GetAxis(m_inputV) * m_Speed;

            //if(m_kickBack == true) // if the player has just shot then kick back will affect the player
            //{
            //    if (z >= 0) // if the player is moving forward or not moving then send the player back a small distance very quickly
            //    {
            //        m_rigidbody.velocity = (m_Transform.forward * 5 * Time.deltaTime * 30) * -1;
            //    }
            //    StartCoroutine(ResetKickback()); // starts a timer to turn off kick back
            //}
            //else // other wise if kick back is not in affect then move normally
            //{
            //    m_rigidbody.velocity = m_Transform.forward * z * Time.deltaTime; // move in the direction the ayer is facing.
            //}

            // float z = Input.GetAxis(m_inputV) * m_Speed;
            
            m_moveInput = new Vector3(Input.GetAxis(m_inputH), 0f, Input.GetAxis(m_inputV));

            if (m_kickBack == true) // if the player has just shot then kick back will affect the player
            {
                if (Input.GetAxis(m_inputV) >= 0 || Input.GetAxis(m_inputH) >= 0) // if the player is moving forward or not moving then send the player back a small distance very quickly
                {
                    m_rigidbody.velocity = (m_Transform.forward * 5 * Time.deltaTime * 30) * -1;
                }
                StartCoroutine(ResetKickback()); // starts a timer to turn off kick back
            }
            else // other wise if kick back is not in affect then move normally
            {
                m_rigidbody.velocity = m_moveInput * m_Speed * Time.deltaTime; // move in the direction the player is facing.
            }

        }
        /// <summary>
        /// after a short time turn kick back off
        /// </summary>
        /// <returns></returns>
        private IEnumerator ResetKickback()
        {
            yield return new WaitForSeconds(0.03f); 

            m_kickBack = false;
        }

        /// <summary>
        /// rotates the player
        /// </summary>
        private void Rotate()
        {
            if (m_ControllerInput == false)
            {
                m_rotationY = m_Transform.rotation.eulerAngles.y; // get current rotation

                m_rotation = m_rotationY + Input.GetAxis(m_inputH) * m_rotationSpeed * Time.deltaTime; // adds new rotation to old to get new rotation

                m_rigidbody.rotation = Quaternion.Euler(new Vector3(0, m_rotation, 0)); //  applys new rotation to rigid body
            }
           

                //Direction to face
             m_dirX = Input.GetAxis(m_inputH);
             m_dirY = Input.GetAxis(m_inputV);

                 m_dirAngle = Mathf.Atan2(m_dirX, m_dirY);
            //USE THIS HERE!!!
            if (m_dirX != 0 || m_dirY != 0) // if theres a rotation 
            {
                float angleD = m_dirAngle * Mathf.Rad2Deg;

                Vector3 targetRot = new Vector3(0, angleD, 0);

                m_rigidbody.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(targetRot), m_rotationSpeed * Time.deltaTime);
            }

            ////mouse use for debug
            //Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));

            //Vector3 mousePosition = Input.mousePosition;
            //mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            //mousePosition = new Vector3(mousePosition.x, 0 , mousePosition.z);


            //transform.LookAt(mousePosition, Vector3.down);
        }

        /// <summary>
        /// This method is responsible for checking to see what projectile has hit the player
        /// </summary>
        /// <param name="other"></param>
        void OnCollisionEnter(Collision other)
        {
            // damage
            if(other.gameObject.CompareTag("Circle"))
            {
                if(m_cycle == 1) // if the player is a circle and is hit by a circle die
                {
                    GameObject projectile = other.gameObject;
                    projectile.GetComponent<ProjectileScript>().DeactivateOnHit();
                    m_health -= 100;
                }          
            }
            else if(other.gameObject.CompareTag("Square")) // if the player is hit by a square and is a square die
            {
                if(m_cycle == 2)
                {
                    GameObject projectile = other.gameObject;
                    projectile.GetComponent<ProjectileScript>().DeactivateOnHit();
                    m_health -= 100;
                }  
            }
            else if(other.gameObject.CompareTag("Triangle")) // if the player is a triangle and is hit by a triangle then die
            {
                if(m_cycle == 3)
                {
                    GameObject projectile = other.gameObject;
                    projectile.GetComponent<ProjectileScript>().DeactivateOnHit();
                    m_health -= 100;
                }
            }

            if(m_health <= 0)
            {
                Die();  
            }   
        }

        /// <summary>
        /// Checks to see what ammo to pick up
        /// </summary>
        /// <param name="other"></param>
        void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("CircleAmmo"))
            {
                if (m_cycle == 1)
                {
                    PlayAmmoPickup();
                    m_ammo[0]++;
                    other.SendMessage("DestroySelf");
                }
            }
            else if(other.CompareTag("SquareAmmo"))
            {
                if (m_cycle == 2)
                {
                    PlayAmmoPickup();
                    m_ammo[1]++;
                    other.SendMessage("DestroySelf");
                }
            }
            else if(other.CompareTag("TriangleAmmo"))
            {
                if (m_cycle == 3)
                {
                    PlayAmmoPickup();
                    m_ammo[2]++;
                    other.SendMessage("DestroySelf");
                }
            }

            gameManger.GetComponent<DisplayAmmo>().UpdateAmmo(m_ammo, name, m_cycle); // updates the ammo
        }

        /// <summary>
        /// checks to see what ammo to pickup. On trigger stay is used so that if the player changes chage while already in the collider they can pick it up.
        /// </summary>
        /// <param name="other"></param>
        void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("CircleAmmo"))
            {
                if (m_cycle == 1)
                {
                    PlayAmmoPickup();
                    m_ammo[0]++;
                    other.SendMessage("DestroySelf");
                }
            }
            else if (other.CompareTag("SquareAmmo"))
            {
                if (m_cycle == 2)
                {
                    PlayAmmoPickup();
                    m_ammo[1]++;
                    other.SendMessage("DestroySelf");    
                }
            }
            else if (other.CompareTag("TriangleAmmo"))
            {
                if (m_cycle == 3)
                {
                    PlayAmmoPickup();
                    m_ammo[2]++;
                    other.SendMessage("DestroySelf");   
                }
            }

            gameManger.GetComponent<DisplayAmmo>().UpdateAmmo(m_ammo, name, m_cycle);
        }

        /// <summary>
        /// fires the projectile if the player has enough ammo
        /// </summary>
        private void Fire()
        {
            if (m_canShoot == true) // check to see if they player isnt colliding with any walls
            {
                if (m_cycle == 1) 
                {
                    if (m_ammo[0] > 0)
                    {
                        GameObject Circle = PoolManager.current.GetPooledObject("Circle");
                        Circle.transform.position = spawnPoint.transform.position;
                        Circle.transform.rotation = spawnPoint.transform.rotation;
                        Circle.GetComponent<ProjectileScript>().GetPlayer(name); // sends the name of the play firing so the projectile can identify which colour to be.
                        Circle.GetComponent<TrailRenderColour>().GetPlayer(name); // sends the name of the player firing so the projectile can set the trail colour accordingly.
                        Circle.SetActive(true);
                        m_ammo[0]--;
                        PlayShotSound();
                        m_kickBack = true; // make kick back take effect
                    }
                }
                else if (m_cycle == 2)
                {
                    if (m_ammo[1] > 0)
                    {
                        GameObject Square = PoolManager.current.GetPooledObject("Square");
                        Square.transform.position = spawnPoint.transform.position;
                        Square.transform.rotation = spawnPoint.transform.rotation;
                        Square.GetComponent<ProjectileScript>().GetPlayer(name); // sends the name of the play firing so the projectile can identify which colour to be.
                        Square.GetComponent<TrailRenderColour>().GetPlayer(name); // sends the name of the player firing so the projectile can set the trail colour accordingly.
                        Square.SetActive(true);
                        m_ammo[1]--;
                        PlayShotSound();
                        m_kickBack = true;
                    }
                }
                else if (m_cycle == 3)
                {
                    if (m_ammo[2] > 0)
                    {
                        GameObject Triangle = PoolManager.current.GetPooledObject("Triangle");
                        Triangle.transform.position = spawnPoint.transform.position;
                        Triangle.transform.rotation = spawnPoint.transform.rotation;
                        Triangle.GetComponent<ProjectileScript>().GetPlayer(name); // sends the name of the play firing so the projectile can identify which colour to be.
                        Triangle.GetComponent<TrailRenderColour>().GetPlayer(name); // sends the name of the player firing so the projectile can set the trail colour accordingly.
                        Triangle.SetActive(true);
                        m_ammo[2]--;
                        PlayShotSound();
                        m_kickBack = true;
                    }
                }
                gameManger.GetComponent<DisplayAmmo>().UpdateAmmo(m_ammo, name, m_cycle);
            }           
        }

        public void SetPaused(bool isPaused)
        {
            m_paused = isPaused;
        }

        private void PlayShotSound()
        {
            m_AudioSource.clip = shootSound;
            m_AudioSource.Play();
        }

        private void PlayAmmoPickup()
        {
            m_AudioSource.clip = AmmoPickup;
            m_AudioSource.Play();
        }

        private void Die()
        {
            if (gameManger)
            {
                gameManger.GetComponent<RoundEnd>().DisplayWinner(name);
            }
            gameObject.SetActive(false);
        }

        public void SetCanShoot(bool _canSHoot)
        {
            m_canShoot = _canSHoot;
        }

        private IEnumerator ResetShootTriggerInput()
        {
            yield return new WaitForSeconds(m_TriggerInputResetTime / 2);
            m_canTriggerShoot = true;
        }

        private IEnumerator ResetSwitchTriggerInput()
        {
            yield return new WaitForSeconds(m_TriggerInputResetTime);
            m_canTriggerSwitch = true;
        }

        private void DetermineInput()
        {
            if (tag == "Player1")
            {
                if (m_ControllerInput == false)
                {
                    m_inputH = "Horizontal1";
                    m_inputV = "Vertical1";
                }
                else
                {
                    m_inputH = "Controller1Horizontal";
                    m_inputV = "Controller1Vertical";
                }

            }
            else if (tag == "Player2")
            {
                if (m_ControllerInput == false)
                {
                    m_inputH = "Horizontal2";
                    m_inputV = "Vertical2";
                }
                else
                {
                    m_inputH = "Controller2Horizontal";
                    m_inputV = "Controller2Vertical";
                }
            }
        }
    }
}

